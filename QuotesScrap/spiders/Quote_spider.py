import scrapy
from ..items import QuotesscrapItem

class QuotesSpider(scrapy.Spider):
    name = 'quotes'
    start_urls = [
        'https://www.indianmotorcycle.com/en-us/shop/accessories/luggage-racks/'
    ]

    def parse(self, response):
        divs = response.css('.plp__list .plp__product')
        for div in divs:
            href = div.css('a.text-decoration-none::attr(href)').extract_first()
            link = response.urljoin(href)
            yield {'linktext': link}
            yield scrapy.Request(link, self.product)

    def product(self, response):
        name = response.css('.sales-widget__product-name::text').extract()
        items = QuotesscrapItem()
        items['name'] = name
        yield items
