from __future__ import print_function
import mysql.connector
from mysql.connector import errorcode


class QuotesscrapPipeline(object):
    table = 'quotes'
    conf = {
        'host': '127.0.0.1',
        'user': 'root',
        'password': '3010',
        'database': 'scrapy',
        'raise_on_warnings': True
    }

    def __init__(self, **kwargs):
        self.cnx = self.mysql_connect()

    def open_spider(self, spider):
        print("spider open")

    def process_item(self, item, spider):
        print("Saving item into db ...")
        self.save(item)
        return item

    def close_spider(self, spider):
        self.mysql_close()

    def mysql_connect(self):
        try:
            return mysql.connector.connect(**self.conf)
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)

    def save(self, item):
        cursor = self.cnx.cursor()
        sql = "INSERT INTO " + self.table + "( title ) VALUES (%s)"
        print(item)
        val = [
            item['name'][0]
        ]
        # Insert new row
        cursor.execute(sql, val)
        lastRecordId = cursor.lastrowid

        # Make sure data is committed to the database
        self.cnx.commit()
        cursor.close()
        print("Item saved with ID: {}".format(lastRecordId))

    def mysql_close(self):
        self.cnx.close()